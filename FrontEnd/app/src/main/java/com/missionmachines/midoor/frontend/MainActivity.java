package com.missionmachines.midoor.frontend;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Random;
import java.util.Scanner;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MainActivity extends AppCompatActivity {

    private static final String _TAG = "AppClient";
    private static final long _STATUS_CHECK_SECONDS = 4;
    private static final long _RETRY_SECONDS = 2;
    private static final long _NO_STATUS_RETRIES = 8;
    private static final long _EVENTS_MAX = 12;
    private static final long _TIMEOUT_MS = 3000;

    Socket _Socket;
    BufferedReader _ReadStream;
    DataOutputStream _WriteStream;

    private Thread _WorkerThread;
    private long _LastLocalIP;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void connecting(View view) {
        /**
         * Verbosity box in the screen of app
         */
        textView = (TextView) findViewById(R.id.text);

        Random me = new Random();
        me.setSeed(11111111);

        textView.setText(me.toString() + "\n");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        SSLSocketFactory sf;
        Scanner scanner;
        String rxMsg;

        TrustManager[] trustAllCerts = new TrustManager[]
                {
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                            }
                            @Override
                            public X509Certificate[] getAcceptedIssuers() {
                                /** return new X509Certificate[0]; */
                                return null;
                            }
                                                                                                                                                                                                                                                      }
                };

        try {
            SSLContext ctx = SSLContext.getInstance("TLS");

            ctx.init(null, trustAllCerts, new java.security.SecureRandom());
            sf = ctx.getSocketFactory();

            // _Socket = sf.createSocket(AppServer, AppServerPort);
            _Socket = sf.createSocket("midoor.missionmachines.com", 23001);
            // _Socket = sf.createSocket("104.131.106.42", 23001);

            _Socket.setSoTimeout(5000);
            _ReadStream = new BufferedReader(new InputStreamReader(_Socket.getInputStream()));
            _WriteStream = new DataOutputStream(_Socket.getOutputStream());

            String step_one = textView.getText()+"\n" + "step One Success Full !";
            // textView.setText(step_one);

        } catch (Exception e){
            // textView.setText(e.toString());
        }

        /**
         * begin communication with the server
         */
        try {
            textView.setText(textView.getText()+"\n"+_Socket.getLocalAddress().getHostAddress().toString());
        } catch (Exception e){
            textView.setText(textView.getText()+ "\n" + e.toString());
        }

        /**
         * begin to communicate with the server by SSL
         */

        // get key from text AppServerUser
        EditText AppKey = (EditText) findViewById(R.id.AppKey);
        String AppKeyMsg = AppKey.getText().toString();

        // get key from text AppServerUser
        EditText AppServerUser = (EditText) findViewById(R.id.AppServerUser);
        String AppServerUserMsg = AppServerUser.getText().toString();

        // get key from text AppServerPassword
        EditText AppServerPassword = (EditText) findViewById(R.id.AppServerPassword);
        String AppServerPasswordMsg = AppServerPassword.getText().toString();

        if(!_Ping("APP HELLO " + AppKeyMsg + "\n")){
            // textView.setText(textView.getText()+"\n * NO: Hello");
        } else {
            // textView.setText(textView.getText()+"\n * YES: Hello");
        }


        if(!_Ping("LOGON " + AppServerUserMsg + " " + AppServerPasswordMsg + "\n")) {
            // textView.setText(textView.getText()+"\n * NO: Logon");
        } else {
            // textView.setText(textView.getText()+"\n * Yes: Logon");
        }

        rxMsg = _ReadUntilCR(_TIMEOUT_MS);
        if (rxMsg == null) {
            textView.setText(textView.getText() + " << No Logon response");
            _Close();
        } else {
            textView.setText(textView.getText() + " || LOGIN");
        }

        if (!rxMsg.equalsIgnoreCase("CONFIRMED")) {
            textView.setText(textView.getText() + " << Logon not configed: " + rxMsg);
            _Close();
        } else {
            textView.setText(textView.getText() + " || CONFIRMED");
        }


        String command = "GET";
        /**
        _v(command + "\n");
        _n(command);
*/

        command = "GET ALARM\n";
        if(!_v(command)) {
            textView.setText(textView.getText() + "\n Error (in GET ALARM) <<--");
        } else {
            textView.setText(textView.getText() + "\nGET ALARM <<--");
        }
        _n(command);

        // get key from text AppServerPassword
        EditText order = (EditText) findViewById(R.id.order);
        String orderMsg = order.getText().toString();

        /**
        command = "NEXT 0: LOG " + orderMsg;
        _v(command + "\n");
        _n(command);

        command = "SET 0: push";
        _v(command + "\n");
        _n(command);
        */
        _Ping("EXIT\n");
    }

    private void _n(String c){
        textView.setText(textView.getText() + " \n" + c + "-------\n");
    }

    private boolean _v(String msg) {
        String rxMsg;
        boolean res = !_Ping(msg);
        if(res) {
            textView.setText(textView.getText() + "\n -- " + msg);
        } else {
            textView.setText(textView.getText() + "\n + " + msg);
        }

        rxMsg = _ReadUntilCR(_TIMEOUT_MS);
        if (rxMsg == null) {
            textView.setText(textView.getText() + " No PUSH response");
        } else {
            textView.setText(textView.getText() + "\n ;;" + rxMsg.toString());
        }
        return res;
    }

    private boolean _Ping(String ping) {
        if(_WriteStream == null) {
            return false;
        }
        try {
            _WriteStream.write(ping.getBytes(), 0, ping.length());
           // textView.setText(textView.getText() + "\n ! _Ping Ok " + _WriteStream.toString());
            _WriteStream.flush();
        } catch (Exception e){
           // textView.setText(textView.getText()+"\n * _Ping Error "+e.toString() + " " + _WriteStream.toString());
            return false;
        }
        return true;
    }

    private String _ReadUntilCR(long timeoutMs) {
        String s = null;
        try {
            s = _ReadStream.readLine();
            while (_ReadStream.ready()){
                s = s +" "+ _ReadStream.readLine();
            }
            textView.setText(textView.getText() + "\n" + s);
            return s;
        } catch (Exception e) {
            textView.setText(textView.getText()+"\n * no read in _ReadUntilCR");
            return s;
        }
    }

    private void _Close()
    {
        _Close(false);
    }

    private void _Close( boolean ignoreStatus) {
        if (!ignoreStatus && _IsConnected()) {
            _Ping("EXIT\n");
        }
        if (_WriteStream != null) {
            try {
                _WriteStream.close();
            } catch (Exception e) {
                //Log.d(_TAG, "Close Write Stream: " + e.getMessage());
            }
            _WriteStream = null;
        }
        if (_ReadStream != null) {
            try {
                _ReadStream.close();
            } catch (Exception e) {
                //Log.d(_TAG, "Close Read Stream: " + e.getMessage());
            }
            _ReadStream = null;
        }
        if (_Socket != null) {
            try {
                _Socket.close();
            } catch (Exception e) {
                //Log.d(_TAG, "Close Socket: " + e.getMessage());
            }
            _Socket = null;
        }
        _ClearState();
    }

    private synchronized boolean _IsConnected()
    {
        if (_WorkerThread == null) {
            return false;
        }
        if (_Socket == null || _ReadStream == null
                || _WriteStream == null) {
            return false;
        }
        /* Check status of socket  */
        if (!_Socket.isConnected() || _Socket.isClosed()
                ||_Socket.isInputShutdown() || !_Socket.isBound()) {
            return false;
        }

        return true;
    }
    private void _ClearState()
    {

    }
}

